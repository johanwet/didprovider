<?php
/**
 * Core functionality helper class.
 * 
 * @author Johan Wettergren
 * @copyright Araneo, 2015
 * @package Helper
 */
class Core {
	
	/**
	 * Get a named HTTP GET/POST parameter.
	 * If it is not set, the defaultValue will be returned. 
	 * 
	 * @param String Parameter name.
	 * @param mixed Default value to return if parameter is not set
	 * @return mixed HTTP Parameter value or default value.
	 */
	public static function getURIParameter($param, $defaultValue=null) {
		if(isset($_REQUEST[$param]) && is_array($_REQUEST[$param]))
			return $_REQUEST[$param];
		else
			return isset($_REQUEST[$param]) ? trim($_REQUEST[$param]) : $defaultValue;
	}

	
	/**
	 * Redirect the user to a new page.
	 * 
	 * @param String New URI to redirect to.
	 */
	public static function redirect($URI) {
		header("Location: {$URI}", true, 302); // 302 Found
		exit;
	}
	
	/**
	 * Validate that a  time string is on xx:xx format and a valid time.
	 * 
	 * @param unknown_type $time
	 * @return bool true on success.
	 */
	public static function validateTime($time) {
		
		$parts = explode(":", $time);
		if(!is_array($parts) || (count($parts) != 2))
			return false;
		if(!preg_match("/^[012][0123456789]$/", $parts[0]))
			return false;
		if(!preg_match("/^[012345][0123456789]$/", $parts[1]))
			return false;
		if((int)$parts[0] > 23)
			return false;
		return true;		
	}

	/**
	 * Validate that a date time string is on YYYY-mm-dd HH:ii:ss format and a valid time.
	 *
	 * @param String Date time
	 * @return Boolean True on success.
	 */
	public static function validateDateTime($time) {	
		return true;
	}
	
	
	public static function formatTime($time) {
		$parts = explode(":", $time);
		return $parts[0] . ":" . $parts[1];
	}
	

	/**
	 * Used in API class to make a generic response object which only sets the statusCode, used for errors.
	 * 
	 * @param String Status Code to set in response object
	 * @return Object API Response object with Status Code set
	 */
	public static function makeGenericApiResponse($code) {
		$ret = new stdClass;
		$ret->statusCode = $code;
		return $ret;
	}
	
	
	/**
	 * Convert seconds to HH:mm:ss
	 * 
	 * @param Integer Seconds to convert
	 * @return String Seconds represented as HH:mm:ss
	 */
	public static function secToTime($sec, $forceHrs=false) {
				
		// Round it off (for direct mt-mt use)
		$sec = round($sec);
		
		// Get hours
		$hrs = floor($sec/(60*60));
		
		// Remove hours from sec
		$sec -= ($hrs*60*60);
		
		// Get minutes
		$min = floor($sec/60);
		
		// Remove minutes
		$sec -= ($min * 60);
		
		$min = str_pad($min, 2, "0", STR_PAD_LEFT);
		$sec = str_pad($sec, 2, "0", STR_PAD_LEFT);

		if($forceHrs)
			$str = str_pad($hrs, 2, "0", STR_PAD_LEFT) . ":{$min}:{$sec}";
		else
			$str = ($hrs == 0 ? "" : (str_pad($hrs, 2, "0", STR_PAD_LEFT) . ":")) . "{$min}:{$sec}";
		return $str;		
		
	}
	
	
	/**
	 * Get the current ISO-8601 DateTime in UTC
	 * 
	 * @return String ISO-8601 DateTime in UTC
	 */
	public static function getUtcDateTime() {
		$dt = new DateTime(null, new DateTimeZone("UTC"));
		return $dt->format("Y-m-d H:i:s");
	}

	
	/**
	 * Convert a local time in a given time zone, to UTC
	 * 
	 * @param String Local time to convert to UTC, in ISO-8601 format
	 * @param String Local time Time Zone
	 * @return String UTC time of local time in ISO-8601 format.
	 */
	public static function convertToUtcDateTime($localTime, $fromTimeZone) {
		$dt = new DateTime($localTime, new DateTimeZone($fromTimeZone));
		$dt->setTimezone(new DateTimeZone("UTC"));
		return $dt->format("Y-m-d H:i:s");
	}
	
	public static function objToArray($obj) {
		$ret = array();
		foreach($obj as $key => $val)
			$ret[$key] = $val;
		return $ret;
	}
}
