<?php

abstract class APITestBase extends PHPUnit_Framework_TestCase {
	
	const WSDL = "http://127.0.0.1/operation.php?wsdl";
	const ENDPOINT = "http://127.0.0.1/operation.php";
	
	// Before Class execution
	public static function setUpBeforeClass() {
		Log::fcn("APITestBase::setUpBeforeClass()");
	}
	
	// Before Test execution
	protected function setUp() {
		Log::fcn("APITestBase::setUp()");
	}
	
	// After each Test
	protected function tearDown() {
		Log::fcn("APITestBase::tearDown()");
		$db = DB::getDB();
		$db->query("DELETE FROM didlock");
		$db->query("UPDATE did SET state='FREE'");
		Log::debug("Truncated table 'didlock'");
	}
	
	// After sucessful Class execution
	public static function tearDownAfterClass() {
		Log::fcn("APITestBase::tearDownAfterClass()");
	}
	
	// After failed Test execution
	protected function onNotSuccessfulTest(Exception $e) {
		Log::fcn("APITestBase::onNotSuccessfulTest(Exception: " . $e->getMessage() . ")");
		throw $e;
	}
	
	/**
	 * Get a SOAP Client for localhost for Web Service Tests.
	 *
	 * @return SoapClient
	 */
	protected static function getClient($username='', $password='') {
		Log::fcn("APITestBase::getClient({$username}, {$password})");
		$api = new SoapClient(self::WSDL, array("encoding" => "utf-8",
				"trace" => true,
				"exceptions" => true,
				"connection_timeout" => 5,
				"cache_wsdl" => WSDL_CACHE_NONE,
				"user_agent" => "PHP SOAP Test Framework",
				"authentication" => SOAP_AUTHENTICATION_BASIC,
				"login" => $username,
				"password" => $password
		));
	
		//if(isset(self::ENDPOINT))
		if(defined(self::ENDPOINT))
			$api->__setLocation(self::ENDPOINT);
		
		return $api;
	}
	
	
}