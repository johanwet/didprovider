<?php
/**
 * The Header Class contains support methods that relates fetching and validating HTTP Headers.
 *
 * @author Johan Wettergren
 * @copyright Araneo, 2015
 * @package Helper
 */
abstract class Header {
	
	/**
	 * Read the Authorization header from the HTTP request.
	 *
	 * @return Array UserKey and Hash Array
	 */
	public static function getAuthorizationHeader() {
	
		Log::fcn("Header::getAuthorizationHeader()");
	
		// Get all the headers
		$headers = apache_request_headers();
		Log::debug("Read HTTP Header: " . print_r($headers, true));
	
		// Read the "Authorization" header
		if(!isset($headers["Authorization"])) {
			Log::error("Invalid HTTP Header, Authorization not set");
			return false;
		}
	
		// Validate the Authorization format USERKEY:BASE64HASH {
		if(!preg_match("/^([A-Z0-9]{8,16}):([A-z0-9=\+\/]{4,})$/", $headers["Authorization"], $authParts)) {
			Log::error("Invalid HTTP Header, Invalid Authorization format", $headers["Authorization"]);
			return false;
		}
	
		// Validate that parts[1,2] exists
		if(!is_array($authParts) || (count($authParts) != 3)) {
			Log::error("Invalid Auth Parts detected: " . print_r($authParts, true));
			return false;
		}
	
		// Get the Parts
		$ret = array("UserKey" 	=> $authParts[1],
						 "Hash" 		=> $authParts[2]);
	
		Log::debug("Read Authorization:" . print_r($ret , true));
		return $ret;
	}
	
	
	/**
	 * Read the Basic Authorization header from the HTTP request.
	 *
	 * @return Array Username and Password Array, or False on failure
	 */
	public static function getBasicAuthenticationHeader() {
	
		Log::fcn("Header::getAuthorizationHeader()");
	
		if(!isset($_SERVER['PHP_AUTH_USER']) || !isset($_SERVER['PHP_AUTH_PW'])) {
			Log::error("HTTP Authorization header not set");
			return false;
		}
	
		// Create the return object
		$ret = array("Username" => $_SERVER['PHP_AUTH_USER'],
						 "Password"	=> $_SERVER['PHP_AUTH_PW']);
	
		Log::debug("HTTP Authentication Header loaded");
		return $ret;
	}
	
	
	/**
	 * Read the Date header from the HTTP request.
	 *
	 * @return String Date string in Header
	 */
	public static function getDateHeader() {
	
		Log::fcn("Header::getDateHeader()");
	
		// Get all the headers
		$headers = apache_request_headers();
	
		// Read the "Date" header
		if(!isset($headers["Date"])) {
			Log::error("Invalid HTTP Header, Date not set");
			return false;
		}
	
		// Validate the date
		if(!Core::validateDate($headers["Date"])) {
			Log::error("Invalid HTTP Header, Invalid Date", $headers["Date"]);
			return false;
		}
	
		Log::debug("Read Date header: " . $headers["Date"]);
		return $headers["Date"];
	}
	
	
	/**
	 * Get the raw HTTP URI of the request
	 *
	 * @return String HTTP URI of request
	 */
	public static function getUri() {
	
		Log::fcn("Header::getUri()");
	
		// Grab the URI
		$requestUri = $_SERVER['REQUEST_URI'];
	
		// Strip the path that the Load Balancer adds
		if(substr($requestUri, 0, 7) == "/public") {
				
			if(API_ENV != "LAB") {
				$requestUri = substr($requestUri, 7);
				Log::info("Stripped LB Path: {$requestUri}");
			} else {
				Log::debug("LAB Environment, no stripping allowed");
			}
		}
	
		Log::debug("REQUEST URI: {$requestUri}");
		return $requestUri;
	}
		
}
