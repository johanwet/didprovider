<?php
/**
 * Interface for ORM-based Models.
 * 
 * @author Johan Wettergren
 * @copyright Araneo, 2014
 * @package Interface
 */
interface ifModel {

	/**
	 * Factory an instance by loading it from the database by the id.
	 * 
	 * @param int Instance id.
	 * @return instance Loaded instance. 
	 */	
	public static function factoryFromId($id);
	
	
	/**
	 * Factory an instance by its parameters from an object.
	 * 
	 * @param object Object with the attributes to load, e.g., a database row.
	 * @param instance Loaded instance.
	 */
	public static function factoryFromObject($obj);

	
	/**
	 * Insert the instance into the backend.
	 * 
	 * @return Integer ID of new Instance on success.
	 * @throws SQLException
	 */
	public function __insert();
	
	 
	/**
	 * Update the current object to the database.
	 * 
	 * @return Boolean True on success.
	 * @throws SQLException
	 */
	public function __update();

	
	/**
	 * Remove the instance from the database.
	 * 
	 * @return Boolean True on success.
	 * @throws SQLException
	 */
	public function __delete();
}
