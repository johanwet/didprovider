<?php
/**
 * Database Singleton Class
 * 
 * @author Johan Wettergren
 * @copyright Araneo, 2015
 * @package Support
 */
class DB {
		
	/** @var resource Mysqli singleton container. */
	private static $db = null;
	
	/** @var resource Memcache singleton container. */
	private static $memcache = null;
	
	
	/**
	 * Get Database connection wrapper.
	 * Calls either getSSLDB or getUnencryptedDB.
	 * @see DB::getSSLDB
	 * @see DB::getUnencryptedDB
	 * 
	 * @param string Database name to use. Defaults to pre-defined db.
	 * @return mysqli DB instance.
	 */
	public static function getDB($database=null) {
		
		Log::dbldebug("DB::getDB({$database})");
		
		// Test if you should connect by SSL
		if(DB_USE_SSL)
			return self::getSSLDB($database);
		else
			return self::getUnencryptedDB($database);
	}
	

	/**
	 * Open a connection to the database with SSL.
	 * @param string Datbase name to use, if not set, use default db.
	 * @return mysqli DB instance.
	 */
	private static function getSSLDB($database=null) {
		
   	//Log::dbldebug("DB::getSSLDB({$database})");
   	
		if(is_null(self::$db)) {

			// Create a mysqli object
			if(!$db = mysqli_init()) {
				Log::error("Could not perform mysqli_init");
				throw new Exception("SSLDB: Could not perform mysqli_ini", 3201);
			}
			
			// Set the SSL state, use the defined PEM
			$db->ssl_set(null, null, DB_SSL_PEM, null, null);

         // Connect to the database over SSL
         if(!$db->real_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_DBASE)) {
         	Log::error("Could not connect to MySQL/SSL database [" . mysqli_connect_errno() . "] " . mysqli_connect_error());
            throw new Exception("SSLDB Could not connect to host", 3202);
			}

      	if(!$db->set_charset("utf8")) {
	      	Log::error("Could not Set UTF-8 charset when connecting to DB: " . $db->error);      
	   	   throw new Exception("SSLDB Could not set UTF-8 charset", 3203);
			}
						
			Log::dbldebug("MySQL connections established with SSL");
			self::$db = $db;
		}
   	
      // Set the database, on request
      if($database) {
      	if(!self::$db->select_db($database)) {
      		Log::error("Could not select database: {$database}");
   	  		throw new Exception("SSLDB Could not select db", 3204);
      	}
      }

      // Return the internal object
      return self::$db;				
	}
	
	
	/**
	 * Open a connection to the database, unencrypted.
	 * @param string Datbase name to use, if not set, use default db.
	 * @return mysqli DB instance.
	 */
   private static function getUnencryptedDB($database=null) {
   
   	Log::dbldebug("DB::getUnencryptedDB({$database})");
   	
      if(is_null(self::$db)) {
	   	
	      // Open db connection.
	      Log::debug("Host=" . DB_HOST);
			self::$db = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_DBASE, DB_PORT);
			
	      // Test for error
	      if(mysqli_connect_errno()) {	
	      	Log::error("Could not connect to MySQL/Plain database [" . mysqli_connect_errno() . "] " . mysqli_connect_error());      
	   	   throw new Exception("Database: Could not connect to host: " . DB_HOST, 3301);
	      }
	      
      	if(!self::$db->set_charset("utf8")) {
	      	Log::error("Could not Set UTF-8 charset when connecting to DB: " . self::$db->error);      
	   	   throw new Exception("Database: Could not set UTF-8 Charset", 3302);
			}
			
	      //Log::dbldebug("MySQL connections established with plain transfer");
      }
   
   
      // Set the database, on request
      if($database) {
      	if(!self::$db->select_db($database)) {
      		Log::error("Could not select database: {$database}");
   	  		throw new Exception("Database: Could not select db", 3303);
      	}
      }

      // Return the internal object
      return self::$db;
   }	
     
	
	/**
	 * Get the memcache instance. Connect if not connected.
	 * 
	 * @return Memcached Connected instance.
	 */
	public static function getMemCache() {

		Log::dbldebug("MC::getInstance()");
		
		// Test if signgleton object is set already,
		if(is_null(self::$memcache)) {

			Log::debug("Creating MC instance");
			
			// Create instance
			self::$memcache = new Memcached;

			// Set option
			// $mc->setOption();
			
			// Add server
			self::$memcache->addServer(MC_SERVER_URI, MC_SERVER_PORT);
			Log::dbldebug("Added MC server: " . MC_SERVER_URI . ":" . MC_SERVER_PORT);
						
		} else {
			Log::dbldebug("Using cached instance");
		}
		
		return self::$memcache;
	}	
	
}
