<?php
/**
 * Log class.
 * 
 * @author Johan Wettergren
 * @copyright Araneo, 2015
 * @package Support
 */
class Log {
         
   /**
    * Log an error.
    * 
    * @param string Error to log.
    * @param string Additional info to log.
    * @return bool True on success.
    */
   public static function error($err, $extrainfo=null) {
   	return self::write($err, $extrainfo, ASCII_RED);
   }

   
   
   /**
    * Log an exception
    *
    * @param Integer Exception code
    * @param String Exception Message
    * @param String Exception Method that threw the exception
    */
   public static function exception($code, $message, $method) {
   	return self::write("[EXCEPTION:{$code}] in {$method}: {$message}", null, ASCII_LRED);
   }
      
   
   /**
    * Log info string.
    * 
    * @param string Command to log.
    * @return bool True on success.
    */
   public static function info($info) {
      return self::write($info, null, ASCII_GREEN);
   }      
   

   /**
    * Log function.
    * 
    * @param string Function to log.
    * @return bool True on success.
    */
   public static function fcn($fcn) {
   	
		if(!LOG_DEBUG_MODE)
   		return true;   	
   	
      return self::write($fcn, null, ASCII_BOLD);
   }      
   

   /**
    * Log a AGI call entry.
    * 
    * @param string AGI function to log.
    * @return bool True on success.
    */
   public static function agi($what) {
   	
		if(!LOG_DEBUG_MODE)
   		return true;
   	  
   	return self::write($what, null, ASCII_PURPLE);
   }      
   
   
   /**
    * Log API function.
    * 
    * @param string API Function to log.
    * @return bool True on success.
    */
   public static function api($fcn) {
   	
		if(!LOG_DEBUG_MODE)
   		return true;   	
   	
      return self::write($fcn, null, ASCII_CYAN);
   }     
   
   
   /**
    * Log a debug string.
    * If the log_debug option is set to false,
    * it will not write to the log.
    * 
    * @param string Debug text to log.
    * @return bool True on success.
    */
   public static function debug($what) {
   	
		if(!LOG_DEBUG_MODE)
   		return true;
   	  
   	return self::write($what, null);
   }   

   
   /**
    * Log a double debug string.
    * Contains alot of data.
    * 
    * @param string Debug text to log.
    * @return bool True on success.
    */
   public static function dbldebug($what) {
      
   	if(!LOG_DBL_DEBUG_MODE)
      	return true;
        
      return self::write($what, null, null);
   }      
   
   
   /**
    * Write to the logfile.
    * If the debug flag is set, it will be sent to stdout too.
    * 
    * @param string Text to log.
    * @param string Ascii color to use.
    * @return bool True on success.
    */
   private static function write($what, $extrainfo=null, $color=null, $file=false) {
   	
   	$open = ($file) ? $file : LOG_TEXT_FILE;
   	
      if($fl = @fopen($open, "a+")) {
      	
      	// Apply the color prefix and suffix
      	if($color) {
      		$pfix = $color;
      		$sfix = ASCII_RESET;
      	} else {
      		$pfix = "";
      		$sfix = "";
      	}
      	
      	// Add the extra info its own line, without color.
      	$putExtra = ($extrainfo ? "{$extrainfo}\n" : "");
      	
         @fwrite($fl, date(LOG_DATE_FORMAT) . " ] {$pfix}{$what}{$sfix}\n{$putExtra}");         
         @fclose($fl);
      }
                 
      return true;
   }         
}
