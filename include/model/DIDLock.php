<?php
/**
 * This class models a DID Lock.
 * 
 * @author Johan Wettergren
 * @copyright Araneo, 2015
 * @package Model
 */
class DIDLock extends Model implements ifModel {

	/**
	 * @var String Underlying table of ORM class.
	 */
	const __TABLE_NAME__ = "didlock";
	
	/**
	 * @var Integer Unique ID of DID which is being locked.  
	 */
	public $didId;

	/**
	 * @var Integer ID of User locking the DID.
	 */
	public $lockedBy;

	/**
	 * @var String Lock Key, used to allocate the DID.
	 */
	public $lockKey;
	
	/**
	 * @var DateTime Date and Time as ISO-8859-1 when DID is being released back as FREE.
	 */
	public $releasedAt;
		
	
	/**
	 * Constructor.
	 * Sufficient information to create a new DIDLock.
	 *
	 * @param Integer Unique ID of DID which is being locked.
	 * @param Integer ID of User locking the DID.
	 * @param String Lock Key, used to allocate the DID 
	 * @param DateTime Date and Time as ISO-8859-1 when DID is being released back as FREE.
	 * @return DIDLock
	 */
	public function __construct($didId, $lockedBy, $lockKey, $releasedAt) {
		$this->didId = $didId;
		$this->lockedBy = $lockedBy;
		$this->lockKey = $lockKey;
		$this->releasedAt = $releasedAt;
	}
	
	
	/**
	 * Factory a DIDLock from its unique id.
	 *
	 * @param Integer ID of DIDLock to spawn
	 * @return DIDLock The assoicated DIDLock, if found, othewise False
	 */
	public static function factoryFromId($id) {
		return self::loadObject(self::__TABLE_NAME__, __CLASS__, "id", $id);
	}
	
		
	/**
	 * Cast a generic object such as a database row to a DIDLock object.
	 * 
	 * @param stdClass Generic object to cast to DIDLock.
	 * @return DIDLock The object casted to a DIDLock instance.
	 */
	public static function factoryFromObject($obj) {
		$dl = new DIDLock($obj->did_id, $obj->locked_by, $obj->lock_key, $obj->released_at);
		$dl->tag($obj);
		return $dl;
	}
	
	
	/**
	 * Return the DID assoicated with the DID Lock
	 * 
	 * @return DID The DID associated witht the DID Lock
	 */
	public function getDid() {
		Log::fcn("DIDLock::getDid() #{$this->id}");
		return DID::factoryFromId($this->didId);
	}
	
	
	/**
	 * Return all Locks currently held for a specific Lock Key Group
	 * 
	 * @param String Unique Lock Key of the locked DIDs
	 * @return Array|DIDLock Set of DIDLock objects associated with the Lock Key
	 */
	public static function getForKey($lockKey) {		
		Log::fcn("DIDLock::getForKey($lockKey)");
		return self::loadList(self::__TABLE_NAME__, __CLASS__, "id", "ASC", "lock_key", $lockKey);
	}
	
	
	/**
	 * Generate a GUID usable as DID Lock Key.
	 * Format: 860a2134-b1bf-42fc-ae42-cdbc4847caec
	 * 
	 * @return String A new hopefully unique string to be used as Lock Key
	 */
	public static function makeKey() {
		Log::fcn("DIDLock::makeKey()");
		$key = substr(sha1(uniqid()), 0, 8) . "-" . substr(sha1(uniqid()), 0, 4) . "-" . substr(sha1(uniqid()), 0, 4) . "-" . substr(sha1(uniqid()), 0, 4) . "-" . substr(sha1(uniqid()), 0, 12);
		Log::info("Created DIDLock Key: {$key}");
		return $key;
	}
	

	/**
	 * Get the number of DIDLock currently held by a User.
	 * 
	 * @param Integer ID of User to get DIDLock Count for.
	 * @throws SQLException
	 * @return Integer Number of DIDLock found for User
	 */
	public static function getLockCount($userId) {
		
		Log::fcn("DIDLock::getLockCount({$userId})");
		
		$db = DB::getDB();
		
		$sql = sprintf("SELECT COUNT(*) AS lock_count FROM didlock WHERE locked_by=%d",
							$db->real_escape_string((int)$userId));

		if(!$resLC = $db->query($sql))
			throw new SQLException(500, "Could not get DID Lock Count, Query Error", $sql, $db->error, __METHOD__);
		
		// Always set for COUNT
		$o = $resLC->fetch_object();
		$lockCount = (int)$o->lock_count;
		
		Log::info("User #{$userId} has {$lockCount} DIDLock(s) currently active");
		return $lockCount;
	}
	
	
	/**
	 * Get a list of DIDLocks which has expired, by UTC time.
	 * 
	 * @throws SQLException On Iinternal Error
	 * @return multitype:DIDLock Array of DIDLocks which can be removed
	 */
	public static function getExpired() {

		Log::fcn("DIDLock::getExpired(()");

		$db = DB::getDB();
		
		$sql = "SELECT * FROM didlock WHERE release_at <= UTC_TIMESTAMP()";
		
		if(!$resDL = $db->query($sql))
			throw new SQLException(500, "Could not get relesable DIDs, Query Error", $sql, $db->error, __METHOD__);
				
		$ret = array();
		while($oDL = $resDL->fetch_object())
			$ret[] = self::factoryFromObject($oDL);
		
		Log::info("Found {$resDL->num_rows} DIDLock(s) to expire");
		return $ret;
	}
	
	
	/**
	 * Insert a DIDLock.
	 *
	 * @see ifModel::__insert()
	 * @throws SQLException On internal error
	 * @return Boolean True on success
	 */
	public function __insert() {
	
		Log::fcn("DIDLock::__insert()");
	
		$db = DB::getDB();
	
		$sql = sprintf("INSERT INTO didlock
							 SET did_id='%s', locked_by='%s', lock_key='%s', released_at='%s', created=UTC_TIMESTAMP()",
							$db->real_escape_string((int)$this->didId),
							$db->real_escape_string((int)$this->lockedBy),
							$db->real_escape_string($this->lockKey),
							$db->real_escape_string($this->releasedAt));
	
		if(!$db->query($sql))
			throw new SQLException(500, "Could not insert DIDLock, Query Error", $sql, $db->error, __METHOD__);
	
		Log::info("DIDLock #{$db->insert_id} inserted for didId={$this->didId} lockedBy={$this->lockedBy} releasedAt={$this->releasedAt}");
		return true;
	}
	
	
	/**
	 * Update a DIDLock in the database,
	 *
	 * @see ifModel::__update()
	 * @throws SQLException On intneral error
	 * @return Boolean True on success
	 */
	public function __update() {
	
		Log::fcn("DIDLock::__update()");
	
		$db = DB::getDB();
	
		$sql = sprintf("UPDATE didlock
							 SET did_id='%s', locked_by='%s', lock_key='%s', released_at='%s', modified=UTC_TIMESTAMP()
							 WHERE id=%d
							 LIMIT 1",
							$db->real_escape_string((int)$this->didId),
							$db->real_escape_string((int)$this->lockedBy),
							$db->real_escape_string($this->lockKey),
							$db->real_escape_string($this->releasedAt),
							$db->real_escape_string((int)$this->id));
	
		if(!$db->query($sql))
			throw new SQLException(500, "Could not update DIDLock, Query Error", $sql, $db->error, __METHOD__);
	
		Log::info("DIDLock #{$this->id} (didId={$this->didId} lockedBy={$this->lockedBy} releasedAt={$this->releasedAt}) updated successfully");
		return true;
	}
	
	
	/**
	 * Delete a DIDLock from the database,
	 *
	 * @see ifModel::__delete()
	 * @throws SQLException On intneral error
	 * @return Boolean True on success
	 */
	public function __delete() {
	
		Log::fcn("DIDLock::__delete()");
	
		$db = DB::getDB();
	
		$sql = sprintf("DELETE FROM didlock WHERE id=%d LIMIT 1",
							$db->real_escape_string((int)$this->id));
	
		if(!$db->query($sql))
			throw new SQLException(500, "Could not delete DIDLock, Query Error", $sql, $db->error, __METHOD__);
	
		Log::info("DIDLock #{$this->id} deleted");
		return true;	
	}	
	
}
