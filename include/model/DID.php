<?php
/**
 * This class models a Direct Inward Dialing, that is, a DID.
 * 
 * @author Johan Wettergren
 * @copyright Araneo, 2015
 * @package Model
 */
class DID extends Model implements ifModel {
	
	/**
	 * @var String Underlying table of ORM class. 
	 */
	const __TABLE_NAME__ = "did";

	/**
	 * @var String ISO-3166-1-alpha-2 country code assoicated with the DID.
	 */	
	public $countryCode;
	
	/**
	 * @var String Area code assoicated with the DID, if any, othewise empty string.
	 */	
	public $areaCode;
	
	/**
	 * @var String The full E.164 number of the DID, without leading zeros or plus sign.
	 */	
	public $e164;
	
	/**
	 * @var Integer DID Owner if any, is NULL for free DIDs.
	 */	
	public $owner;
	
	/**
	 * @var String Current state of the DID. Allowed: FREE,LOCKED,IN_USE,RESERVED,QUARANTINED,PORTED_OUT.
	 */	
	public $state;


	/**
	 * Constructor.
	 * Sufficient information to create a new DID.
	 * 
	 * @param String ISO-3166-1-alpha-2 country code assoicated with the DID
	 * @param String Area code assoicated with the DID, if any, othewise empty string
	 * @param String The full E.164 number of the DID, without leading zeros or plus sign
	 * @param Integer DID Owner if any, is NULL for free DIDs
	 * @param String Current state of the DID
	 * @return DID
	 */
	public function __construct($countryCode, $areaCode, $e164, $owner, $state) {
		$this->countryCode = $countryCode;
		$this->areaCode = $areaCode;
		$this->e164 = $e164;
		$this->owner = $owner;
		$this->state = $state;		
	}

	
	/**
	 * Factory a DID from its unique id. 
	 * 
	 * @param Integer ID of DID to spawn
	 * @return DID The assoicated DID, of found, othewise False
	 */
	public static function factoryFromId($id) {
		return self::loadObject(self::__TABLE_NAME__, __CLASS__, "id", $id);
	}

	
	/**
	 * Factory a DID from its unique E.164 number.
	 *
	 * @param String E.164 number of DID to spawn
	 * @return DID The assoicated DID, of found, othewise False
	 */	
	public static function factoryFromE164($e164) {
		return self::loadObject(self::__TABLE_NAME__, __CLASS__, "e164", $e164);
	}
	
	
	/**
	 * Cast a generic object such as a database row to a DID object.
	 * @param stdClass Generic object to cast to DID
	 * @return DID The object casted to a DID instance
	 */
	public static function factoryFromObject($obj) {
		$d = new DID($obj->country_code, $obj->area_code, $obj->e164, $obj->owner, $obj->state);
		$d->tag($obj);
		return $d;
	}
	
	
	/**
	 * Test if a DID is in state FREE and can be allocated.
	 *  
	 * @param String E.164 of the DID to test state for
	 * @return Boolean True if the DID exists and is in state FREE
	 */
	public static function isFree($e164) {		
		Log::fcn("DID:isFree({$e164})");
		$d = self::factoryFromE164($e164);
		// introduce as test
		// if(!$d)
		// 	throw new SoapFault($faultcode, $faultstring, $faultactor, $detail, $faultname, $headerfault);
		return $d->state == 'FREE' ? true : false;
	}

	
	/**
	 * Test if a DID is in the LOCKED state
	 *
	 * @return Boolean True id the DID is in the LOCKED state
	 */
	public function isLocked() {
		Log::fcn("DID:isLocked() #{$this->id}");		
		return $this->state == 'LOCKED' ? true : false;
	}
	
	
	/**
	 * Set the DID as Locked.
	 * 
	 * @return Boolean True on success
	 */
	public function setLock() {
		$this->state = "LOCKED";
		return $this->__update();
	}
	
	
	/**
	 * Release the DID as FREE, or to Quarantine at a later time.
	 * 
	 * @param string Set to NULL for FREE now, or a date in the future for Quarantine. Not implemented.
	 * @return Boolean True on success
	 */
	public function release($time=null) {
		Log::fcn("DID::release({$time}) #{$this->id}");
		$this->state = "FREE";
		return $this->__update();
	}
	

	/**
	 * Create a DID Lock for the DID.
	 * It's valid for 5 minutes.
	 * 
	 * @param Integer ID of User to lock the DID for
	 * @param String The DIDLockKey to use to lock the DID
	 * @return Boolean True on success
	 */
	public function applyLock($userId, $lockKey) {
		
		Log::fcn("DID::applyLock({$userId}, {$lockKey}) #{$this->id}");

		// Set the lock time to 15 minutes from now
		$dt = new DateTime(null, new DateTimeZone("UTC"));
		$dt->add(new DateInterval("PT300S"));
		$releasedAt = $dt->format('Y-m-d H:i:s');
		
		$dl = new DIDLock($this->id, $userId, $lockKey, $releasedAt);
		$dl->__insert();
		
		// Set self as Locked
		$this->setLock();
		
		Log::debug("DID #{$this->id} ({$this->e164}) locked by User #{$userId} until {$releasedAt}");
		return true;
	}
	
	
	/**
	 * Reserve a set of DIDs, to be allocated later on.
	 * The DIDs should be released after 15 minutes.
	 * 
	 * @param String ISO-3166-1-alpha-2 of the country to reserve DIDs in
	 * @param String Area Code of the area to reserve DIDs in, set to empty string for non-geo
	 * @param Integer Number of DIDs to reserve
	 * @throws SQLException On internal error
	 * @return multitype:DID Set of DIDs to select from to allocate
	 */
	public static function reserve($countryCode, $areaCode, $amount) {
		
		Log::fcn("DID::reserve({$countryCode}, {$areaCode}, {$amount})");

		$db = DB::getDB();
		$sql = sprintf("SELECT * FROM did WHERE country_code='%s' AND area_code='%s' AND state='FREE' ORDER BY RAND() LIMIT %d",
							$db->real_escape_string($countryCode),
							$db->real_escape_string($areaCode),
							$db->real_escape_string((int)$amount));
		
		if(!$resDid = $db->query($sql))
			throw new SQLException(3001, "Could not select did for reservation, query error", $sql, $db->error, __METHOD__);

		// Cast to DID objects
		$ret = array();
		while($oD = $resDid->fetch_object())
			$ret[] = self::factoryFromObject($oD);
		
		
		
		Log::info("Fetched {$resDid->num_rows} free DIDs for country={$countryCode} and area={$areaCode} (requested={$amount})");
		return $ret;
	}
	
	
	/**
	 * Allocate a free DID for a User.
	 * 
	 * @param Integer ID of User which is allocating the DID
	 * @param Integer ID of Free DID to allocate
	 * @param String Allocation Key from DID::reserve
	 * @throws Exception
	 * @return DID The updated DID object
	 * @see DID::reserve
	 */
	public static function allocate($userId, $didId, $key) {

		Log::fcn("DID::allocate({$userId}, {$didId}, {$key})");
		
		// Load the DID
		if(!$did = self::factoryFromId($didId))
			throw new Exception("DID not found", 404);
		
		// Make sure it's LOCKED
		if($did->state != "LOCKED")
			throw new Exception("Provided DID is not in state LOCKED, is '{$did->state}'", 400);
				
		// Update it...
		$did->state = "IN_USE"; // @todo Validate, DID should be in LOCKED
		$did->owner = $userId;
		
		// ...and save it...
		$did->__update();
		
		// ...and return it!
		Log::info("DID #{$didId} allocated by user #{$userId}");
		return $did;
	}
	
	
	/**
	 * Insert a DID in an unallocated state. 
	 * 
	 * @see ifModel::__insert()
	 * @throws SQLException On internal error
	 * @return Boolean True on success
	 */
	public function __insert() {
		
		Log::fcn("DID::__insert()");
		
		$db = DB::getDB();
		
		$sql = sprintf("INSERT INTO did 
							 SET country_code='%s', area_code='%s', e164='%s', state='FREE', created=UTC_TIMESTAMP()",
							 $db->real_escape_string($this->countryCode),
							 $db->real_escape_string($this->areaCode),
							 $db->real_escape_string($this->e164));

		if(!$db->query($sql))
			throw new SQLException(500, "Could not insert DID, Query Error", $sql, $db->error, __METHOD__);
		
		Log::info("DID #{$db->insert_id} inserted for country={$this->countryCode} area={$this->areaCode} e164={$this->e164}");
		return true;
	}
	
	
	/**
	 * Update a DID in the database,
	 *
	 * @see ifModel::__update()
	 * @throws SQLException On intneral error
	 * @return Boolean True on success
	 */	
	public function __update() {

		Log::fcn("DID::__update()");
		
		$db = DB::getDB();
		
		$sql = sprintf("UPDATE did
							 SET country_code='%s', area_code='%s', e164='%s', owner=%s, state='%s', modified=UTC_TIMESTAMP()
							 WHERE id=%d
							 LIMIT 1",
							 $db->real_escape_string($this->countryCode),
							 $db->real_escape_string($this->areaCode),
							 $db->real_escape_string($this->e164),
							 is_null($this->owner) ? "NULL" : ("'" . $db->real_escape_string($this->owner) . "'"),
							 $db->real_escape_string($this->state),
							 $db->real_escape_string($this->id));
		
		if(!$db->query($sql))
			throw new SQLException(500, "Could not update DID, Query Error", $sql, $db->error, __METHOD__);
		
		Log::info("DID #{$this->id} updated successfully");
		return true;		
	}
	

	/**
	 * Delete a DID from the database,
	 * 
	 * @see ifModel::__delete()
	 * @throws SQLException On intneral error
	 * @return Boolean True on success
	 */
	public function __delete() {

		Log::fcn("DID::__delete()");
		
		$db = DB::getDB();
		
		$sql = sprintf("DELETE FROM did WHERE id=%d LIMIT 1",
							$db->real_escape_string((int)$this->id));
		
		if(!$db->query($sql))
			throw new SQLException(500, "Could not delete DID, Query Error", $sql, $db->error, __METHOD__);

		Log::info("DID #{$this->id} deleted");
		return true;
		
	}

}
