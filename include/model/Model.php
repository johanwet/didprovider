<?php
/**
 * Abstract Model class.
 * 
 * @author Johan Wettergren
 * @copyright Araneo, 2015
 * @package Model
 */
abstract class Model {
	
	/**
	 * @var Integer Unique instance id of the deployed Model.
	 */
	public $id;
	
	/**
	 * @var DateTime Time of deployment, ISO-8601 UTC.
	 */	
	public $created;

	/**
	 * @var DateTime Time last modification, if any, ISO-8601 UTC
	 */
	public $modified = null;
	
	
	/**
	 * Magic-method for printing object.
	 * 
	 * @return String Representation of the object.
	 */
	public function __toString() {
		return "[" . get_class($this) . " Instance #{$this->id}]";
	}	
		
	
	/**
	 * Tag an instance with the base data.
	 * Used in the autoloader
	 *
	 * @return void
	 */
	public function tag($obj) {
		$this->id 			= (int)$obj->id;
		$this->created 	= $obj->created;
		$this->modified 	= $obj->modified;
	}
	
		
	/**
	 * Load a unique row from a table by its Auto Increment ID.
	 * Can be used when loading objects to cast to instances.
	 *
	 * @param String Table name to load from
	 * @param Integer Unique numeric id to load in table
	 * @return Object Loaded mysqli object, or False on not found or failue
	 */
	public static function loadObjectById($tblName, $id) {
		Log::fcn("Model::loadObject({$tblName}, {$id})");
		return self::loadObject($tblName, "id", (int)$id);
	}
	
	
	/**
	 * Load a unique row from a table by its unique key.
	 * The row is casted to a suitable intance.
	 *
	 * @param String Table name to load from
	 * @param String Class name to cast to
	 * @param String The name of the column that contains the single primary key
	 * @param String Unique id to load in table, numeric or stirng.
	 * @return Object New instance from row, or False on not found or failue
	 * @throws SQLException
	 */
	protected static function loadObject($tblName, $className, $uniqueColumn, $id) {
	
		Log::fcn("Model::loadObject({$tblName}, {$className}, {$uniqueColumn}, {$id})");
	
		// Get the Database instance
		$db = DB::getDb();
	
		// Build the load query
		$qLoadObj = sprintf("SELECT * FROM %s WHERE %s='%s' LIMIT 1", 
									$db->real_escape_string($tblName), 
									$db->real_escape_string($uniqueColumn), 
									$db->real_escape_string($id));
	
		// Try to load it
		if(!$resObj = $db->query($qLoadObj))
			throw new SQLException(23, "Could not load object #{$id} in table '{$tblName}' by column '{$uniqueColumn}'", $qLoadObj, $db->error, __METHOD__);
	
		// It should be one row
		if($resObj->num_rows != 1) {
			Log::error("Could not load object #{$id} in table '{$tblName}' by column '{$uniqueColumn}', unique id not found or non-unique: {$resObj->num_rows}");
			return false;
		}
	
		// So its one row, good, no need to verify this operation then(?)
		$oObj = $resObj->fetch_object();
	
		// Cast to suitable object
		$instance = forward_static_call(array($className, "factoryFromObject"), $oObj);
	
		// All good
		Log::debug("Loaded database row for object '{$id}' in table '{$tblName}' by column '{$uniqueColumn}' casted to: " . print_r($instance, true));
		return $instance;
	}
	
	
	/**
	 * Load all entities of a table, with custom sorting.
	 *
	 * @param String Table to load frmo
	 * @param String Column to sort by, or NULL for default sorting
	 * @param String Sort order {ASC, DESC} or NULL for default sorting
	 * @return Array Set of matching and sorted Objects, or False on failure
	 * @throws SQLException
	 */
	protected static function loadList($tblName, $className, $sortColumn=null, $sortOrder=null, $key=null, $value=null) {
	
		Log::fcn("Model::loadList({$tblName}, {$className}, {$sortColumn}, {$sortOrder}, {$key}, {$value})");
	
		// Get a DB instance
		$db = DB::getDB();
	
		// Create column filter
		$addFilter = is_null($key) ? "" : sprintf("WHERE %s='%s'", $db->real_escape_string($key), $db->real_escape_string($value));
	
		// Create column sorter
		$addSortCol = is_null($sortColumn) ? "" : sprintf("ORDER BY %s", $db->real_escape_string($sortColumn));
	
		// Create column order sorter, must be {ASC, DESC} or discarded
		$addSortOrder = (!is_null($sortOrder) && in_array($sortOrder, array("ASC", "DESC"))) ? $sortOrder : "";
	
		// Build the query
		$qGetList = sprintf("SELECT *
									FROM %s
									{$addFilter}
									{$addSortCol} {$addSortOrder}",
									$db->real_escape_string($tblName));
	
		// Execute the query, or bail
		if(!$resTbl = $db->query($qGetList))
			throw new SQLException(24, "Could not Load List from table '{$tblName}' with sortCol={$sortColumn} and sortOrder={$sortOrder}", $qGetList, $db->error, __METHOD__);
	
		// All OK
		Log::dbldebug("Loaded {$resTbl->num_rows} row(s) from table '{$tblName}' sorted by column '{$sortColumn}' in sort order '{$sortOrder}'");
	
		// Iterate the result set and cast to suitable object
		$ret = array();
		while($oT = $resTbl->fetch_object())
			$ret[] = forward_static_call(array($className, "factoryFromObject"), $oT);
	
		// Return the objects
		Log::dbldebug("Load List response: " . print_r($ret, true));
		return $ret;
	}	
	
}
