<?php
/**
 * This class models a API User.
 * 
 * @author Johan Wettergren
 * @copyright Araneo, 2015
 * @package Model
 */
class User extends Model implements ifModel {

	/**
	 * @var String Underlying table of ORM class.
	 */
	const __TABLE_NAME__ = "user";
	
	/**
	 * @var String Full Name of User.
	 */
	public $name;
	
	/**
	 * @var String Name of Company assoicated with User.
	 */
	public $company;
	
	/**
	 * @var String Unique login name for User.
	 */
	public $username;
	
	/**
	 * @var String Salted SHA-1 hash of User.
	 */
	public $passwordHash;
	
	/**
	 * @var String Current state of User. Allowed: 'ACTIVE', 'DISABLED', 'TERMINATED'.
	 */
	public $state;
	
	/**
	 * @var Integer Maximum number of locked DIDs at the same time, for the User.
	 */
	public $maxDidLocks;

	
	/**
	 * Constructor.
	 * Sufficient information to create a new User.
	 *
	 * @param String String Full Name of User 
	 * @param String Name of Company assoicated with User
	 * @param String Unique login name for User
	 * @param String Salted SHA-1 hash of User
	 * @param String Current state of User. Allowed: 'ACTIVE', 'DISABLED', 'TERMINATED'
	 * @param Integer Maximum number of locked DIDs at the same time, for the User
	 * @return User
	 */
	public function __construct($name, $company, $username, $passwordHash, $state, $maxDidLocks) {
		$this->name = $name;
		$this->company = $company;
		$this->username = $username;
		$this->passwordHash = $passwordHash;
		$this->state = $state;
		$this->maxDidLocks = $maxDidLocks;
	}
	
	
	/**
	 * Factory a User from its unique id.
	 *
	 * @param Integer ID of User to spawn
	 * @return User The assoicated User, if found, othewise False
	 */
	public static function factoryFromId($id) {
		return self::loadObject(self::__TABLE_NAME__, __CLASS__, "id", $id);
	}
	
		
	/**
	 * Cast a generic object such as a database row to a User object.
	 * 
	 * @param stdClass Generic object to cast to User.
	 * @return User The object casted to a User instance.
	 */
	public static function factoryFromObject($obj) {
		$u = new User($obj->name, $obj->company, $obj->username, $obj->password_hash, $obj->state, $obj->max_did_locks);
		$u->tag($obj);
		return $u;
	}

	
	/**
	 * Load a user from it's unique username and assoicated password hash.
	 * Enforce that the loaded user should be ACTIVE is optional.
	 * 
	 * @param String Unique username of User to load.
	 * @param String Salted SHA-1 password of user to load.
	 * @param Boolean Set to True to enforce that the loaded user must be ACTIVE. Defaults to True.
	 * @throws SQLException
	 * @return boolean|User The loaded User of found, otherwise False
	 */
	public static function factoryFromUserPasswordHash($username, $passwordHash, $forceActive=true) {

		Log::fcn("User::factoryFromActiveUserPasswordHash({$username}, {$passwordHash})");
		
		$db = DB::getDB();
		
		// Test if state=ACTIVE should be enforced
		$sqlActive = "";
		if($forceActive)
			$sqlActive = " AND state='ACTIVE' ";
		
		$sql = sprintf("SELECT * 
							 FROM user 
							 WHERE username='%s' AND 
									 passowrd_hash='%s'
									 %s
							 LIMIT 1",
							$db->real_escape_string($username),
							$db->real_escape_string($passwordHash),
							$sqlActive);
		
		if(!$resUsr = $db->query($sql))
			throw new SQLException(404, "Could not load User by username/hash, Query Error", $sql, $db->error, __METHOD__);
		
		if(!$oU = $resUsr->fetch_object()) {
			Log::info("User with username={$username} and hash={$passwordHash} (force active=" . (int)$forceActive . ") Not Found");
			return false;
		}
		
		Log::info("User #{$oU->id} found for username={$username} and hash={$passwordHash} (force active=" . (int)$forceActive . ") Not Found");
		return self::factoryFromObject($oU);
	} 
	
	
	/**
	 * Insert a User.
	 *
	 * @see ifModel::__insert()
	 * @throws SQLException On internal error
	 * @return Boolean True on success
	 */
	public function __insert() {
	
		Log::fcn("User::__insert()");
	
		$db = DB::getDB();
	
		$sql = sprintf("INSERT INTO user
							 SET name='%s', company='%s', username='%s', password_hash='%s', state='%s', max_did_locks=%d, 
								  created=UTC_TIMESTAMP()",
							$db->real_escape_string($this->name),
							$db->real_escape_string($this->company),
							$db->real_escape_string($this->username),
							$db->real_escape_string($this->passwordHash),
							$db->real_escape_string($this->state),
							$db->real_escape_string((int)$this->maxDidLocks));				
	
		if(!$db->query($sql))
			throw new SQLException(500, "Could not insert User, Query Error", $sql, $db->error, __METHOD__);
	
		Log::info("User #{$db->insert_id} inserted for name={$this->name} company={$this->company} username={$this->username}");
		return true;
	}
	
	
	/**
	 * Update a User in the database,
	 *
	 * @see ifModel::__update()
	 * @throws SQLException On intneral error
	 * @return Boolean True on success
	 */
	public function __update() {
	
		Log::fcn("User::__update()");
	
		$db = DB::getDB();
	
		$sql = sprintf("UPDATE user
							 SET name='%s', company='%s', username='%s', password_hash='%s', state='%s', max_did_locsk=%d, 
								  modified=UTC_TIMESTAMP()
							 WHERE id=%d
							 LIMIT 1",
							$db->real_escape_string($this->name),
							$db->real_escape_string($this->company),
							$db->real_escape_string($this->username),
							$db->real_escape_string($this->passwordHash),				
							$db->real_escape_string($this->state),
							$db->real_escape_string((int)$this->maxDidLocks),
							$db->real_escape_string($this->id));
	
		if(!$db->query($sql))
			throw new SQLException(500, "Could not update User, Query Error", $sql, $db->error, __METHOD__);
	
		Log::info("User #{$this->id} ({$this->username}) updated successfully");
		return true;
	}
	
	
	/**
	 * Delete a User from the database,
	 *
	 * @see ifModel::__delete()
	 * @throws SQLException On intneral error
	 * @return Boolean True on success
	 */
	public function __delete() {
	
		Log::fcn("User::__delete()");
	
		$db = DB::getDB();
	
		$sql = sprintf("DELETE FROM user WHERE id=%d LIMIT 1",
							$db->real_escape_string((int)$this->id));
	
		if(!$db->query($sql))
			throw new SQLException(500, "Could not delete User, Query Error", $sql, $db->error, __METHOD__);
	
		Log::info("User #{$this->id} deleted");
		return true;
	
	}	
	
}
