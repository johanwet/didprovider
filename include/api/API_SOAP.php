<?php
/**
 * Mock-up DID Provider API Class.
 * 
 * @author Johan Wettergren
 * @copyright Araneo, 2015
 * @package API
 */
class API_SOAP {
	
	// getUserAllocations
	
	// getLockedDidsForUser
	
	
	/**
	 * This is a dummy method to test the Authentication
	 * 
	 * @return stdClass A generic object with some server params
	 * @throws Exception On invalid authentication
	 */
	public static function ping() {

		Log::api("API_SOAP::ping()");
		
		// Authenticate User, get User object
		$usr = BasicAuth::authenticate();
		
		$ret = new stdClass;
		$ret->quiterandom = uniqid('', true);
		$ret->servertime = date('c');
		$ret->servertz = date('T');
		
		Log::debug("Ping Response Object: " . print_r($ret, true));
		return $ret;
	}
	
	
	/**
	 * Lock a Range of DIDs so they can be allocated later on.
	 * 
	 * @param String ISO-3166-1-alpha-2 code of the country to lock DIDs in
	 * @param String Area Code in which to get DIDs, set to empty string for non-geo
	 * @param Integer Number of DIDs to lock
	 * @return multitype:DID Set of random DIDs in Country and Area available for allocation
	 */
	public static function lockRange($countryCode, $areaCode, $amount) {
				
		Log::api("API_SOAP::lockRange({$countryCode}, {$areaCode}, {$amount})");

		// Authenticate User, get User object
		$usr = BasicAuth::authenticate();
		
		// You can't allocate more than 5 at the time
		if($amount >  15)
			throw new Exception("Trying to allocate too many DIDs at the same time, the maximum allowed is: 5", 401);
				
		// Check max_lock rule
		$lockCount = DIDLock::getLockCount($usr->id);
		if($lockCount > $usr->maxDidLocks)
			throw new Exception("User #{$usr->id} has {$lockCount} DID Locks, when its maximum amount is: {$usr->maxDidLocks}, try later", 401);
		
		// User is authed and is allowed to lock more DIDs, go ahead
		$arrFreeDid = DID::reserve($countryCode, $areaCode, $amount);
		
		// Make a lock key
		$lockKey = DIDLock::makeKey();
		
		// Apply the lock
		foreach($arrFreeDid as $did)
			$did->applyLock($usr->id, $lockKey);
		
		Log::info("Selected and locked {$amount} DIDs in {$countryCode}/{$areaCode} for User #{$usr->id} ({$usr->name})");
		
		// Create a return object of type tns:didLockList
		$ret = new stdClass;
		$ret->lockKey = $lockKey;
		$ret->dids = $arrFreeDid;
		
		return $ret;		
	}

	
	/**
	 * Allocate a DID.
	 * - It must be in status "FREE".
	 * - All other locked DIDs will be released once a DID is allocated.
	 * 
	 * @param Integer ID of the DID to allocate
	 * @param String The Allocation Key from lockRange
	 * @return DID The allocated DID
	 * @throws Exception
	 */
	public static function allocateDID($didId, $key) {		
		
		Log::api("API_SOAP::allocateDID({$didId}, {$key})");
		
		// Authenticate User, get User object
		$usr = BasicAuth::authenticate();
		
		return DID::allocate($usr->id, $didId, $key);
	}
		
	
	
	/**
	 * Release all DIDs currently locked.
	 * 
	 * @param String Unique Lock Key to release DIDs for
	 * @return Integer Number of released DIDs
	 * @throws Exception
	 */
	public static function releaseLock($key) {

		Log::api("API_SOAP::releaseLock({$key})");
		
		// Authenticate User, get User object
		$usr = BasicAuth::authenticate();
		
		// Make sure that this user actually has any DIDs to releaes
		if(DIDLock::getLockCount($usr->id) == 0) {
			Log::info("User #{$usr->id} does not have any locked DIDs, nothing to do");
			return 0;
		}
		
		// Just a pretty counter
		$released = 0;
		
		foreach(DIDLock::getForKey($key) as $didLock) {
			
			// Make sure that the ownership is kosher
			if($didLock->lockedBy != $usr->id) {
				Log::error("User #{$usr->id} is trying to release DIDLock #{$didLock->id} owned by {$didLock->lockedBy}, skipping");
				continue;
			}

			// Load the associated did
			$did = $didLock->getDid();
			
			// If the DID is not consumed and still LOCKED, set it back to FREE
			// Only up the counter here, we're only interested in the number that were actually released back to the Pool
			if($did->isLocked()) {
				$did->release();
				$released++;
			}
			
			// Dump the lock!
			$didLock->__delete();
		}
		
		Log::info("Deleted {$released} DID Lock(s) for User #{$usr->id} using Lock Key '{$key}'");
		return $released;
	}
	
}

