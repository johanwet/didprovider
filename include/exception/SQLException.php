<?php
/**
 * Exception class for SQL Errors.
 *
 * @author Johan Wettergren
 * @copyright Araneo, 2015
 * @package Exception
 */
class SQLException extends Exception {

	/**
	 * Throw a new SQL Exception, with Logging.
	 *
	 * @param Integer Exception Code
	 * @param String Descriptive message
	 * @param String The SQL Query
	 * @param String SQL Error Message
	 * @param String Exception Method
	 * @throws RestException 
	 */
	public function __construct($code, $message, $query, $error, $method) {
		$query = preg_replace("/\s+/", " ", $query);
		Log::exception($code, $message . " \nQuery: {$query}\nSQL Error: {$error}", $method);
		parent::__construct($message, (int)$code);
	}
		
}
