<?php
/**
 * Exception class for API.
 * Re-uses the Restler RestException class, but adds logging.
 * 
 * @author Johan Wettergren
 * @copyright Araneo, 2014
 * @package Exception
 */
class WebRestException extends RestException {

	/**
	 * Throw a new Exception, with Logging.
	 * 
	 * @param Integer Exception Code
	 * @param String Exception Message
	 * @param String Exception Method
	 */
	public function __construct($code, $message, $method) {
		Log::exception($code, $message . " (@" . number_format((microtime(true)-$GLOBALS['tic'])*1000, 1, '.', '') . " ms)", $method);
		parent::__construct($code, $message);
	}
	
}
