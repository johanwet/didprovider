<?php

// Standard Server settings
date_default_timezone_set("Europe/Stockholm");
error_reporting(E_ALL);
ini_set("display_errors", false); 

// Define where we are for the Loader
define("INCLUDE_ROOT_DIR", dirname(__FILE__));

// Load the correct config
switch(php_uname('n')) {
	
	// Lab
	case 'centos66-01':
		require_once realpath(dirname(__FILE__) . "/../config/conf_lab.php");
		break;
		
	// Integration
	case 'ip-172-30-4-232':
		require_once realpath(dirname(__FILE__) . "/../config/conf_integration.php");
		break;
		
	// Build. Changes for each new EC2 instance.
	default:
		require_once realpath(dirname(__FILE__) . "/../config/conf_builder.php");
		break;
		
}

// Load the ascii
require_once realpath(dirname(__FILE__) . "/ascii.php");


/////////////////////
//// AUTO-LOADER ////
/////////////////////

/**
 * Auto-loader.
 *
 * @param String Class Name to Load
 */
spl_autoload_register('lisasoap_10_autoloader');
function lisasoap_10_autoloader($className) {

	if(file_exists(INCLUDE_ROOT_DIR . "/api/{$className}.php")) {
		require_once INCLUDE_ROOT_DIR . "/api/{$className}.php";

	} elseif(file_exists(INCLUDE_ROOT_DIR . "/auth/{$className}.php")) {
		require_once INCLUDE_ROOT_DIR . "/auth/{$className}.php";

	} elseif(file_exists(INCLUDE_ROOT_DIR . "/com/{$className}.php")) {
		require_once INCLUDE_ROOT_DIR . "/com/{$className}.php";

	} elseif(file_exists(INCLUDE_ROOT_DIR . "/exception/{$className}.php")) {
		require_once INCLUDE_ROOT_DIR . "/exception/{$className}.php";
			
	} elseif(file_exists(INCLUDE_ROOT_DIR . "/interface/{$className}.php")) {
		require_once INCLUDE_ROOT_DIR . "/interface/{$className}.php";

	} elseif(file_exists(INCLUDE_ROOT_DIR . "/model/{$className}.php")) {
		require_once INCLUDE_ROOT_DIR . "/model/{$className}.php";

	} elseif(file_exists(INCLUDE_ROOT_DIR . "/responder/{$className}.php")) {
		require_once INCLUDE_ROOT_DIR . "/responder/{$className}.php";

	} elseif(file_exists(INCLUDE_ROOT_DIR . "/test/{$className}.php")) {
		require_once INCLUDE_ROOT_DIR . "/test/{$className}.php";
		
			
	} elseif(file_exists(INCLUDE_ROOT_DIR . "/{$className}.php")) {
		require_once INCLUDE_ROOT_DIR . "/{$className}.php";
	}	
}

