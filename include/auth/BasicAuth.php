<?php
/**
 * HTTP Basic Authentication class.
 * 
 * @author Johan Wettergren
 * @copyright Araneo, 2015
 * @package Authentication
 */
abstract class BasicAuth {

	/**
	 * Try to load a User from the provided HTTP Basic Authentication header.
	 * User must be in Active state.
	 * 
	 * @throws Exception
	 * @return User If found, and the User is in Active state.
	 */
	public static function authenticate() {
		
		// @todo Make it throw nice 403 Forbidden!
		
		Log::fcn("BasicAuth::authenticate()");

		// Validate that the header is set
		if(!isset($_SERVER['PHP_AUTH_USER']))
			throw new Exception("Failed to validate User '{$username}', No Authenticaton Header Found", 401);
				
		$db = DB::getDB();
		
		// Get the Authentication header, apply the hasing and static salting
		$username = $_SERVER['PHP_AUTH_USER'];
		$pxPassword = $_SERVER['PHP_AUTH_PW'];
		$passwordHash = sha1("Left4Salt" . $pxPassword . "Right#Salt");
		
		// Try to fetch the associated User
		$qUsr = sprintf("SELECT * FROM user WHERE username='%s' AND password_hash='%s' LIMIT 1",
							 $db->real_escape_string($username),
							 $db->real_escape_string($passwordHash));
		
		if(!$resUsr = $db->query($qUsr))
			throw new Exception("Failed to validate User '{$username}', Internal Server Error", 500);
		
		if(!$oU = $resUsr->fetch_object())
			throw new Exception("Failed to validate User '{$username}', Not Found", 404);
				
		// Cast to User object and make sure it's active
		// This can be done in the SQL statement, but then failed attempts are not picked up
		$usr = User::factoryFromObject($oU);
		if($usr->state != "ACTIVE")		
			throw new Exception("Failed to validate User '{$username}', Is not ACTIVE, is '{$usr->state}'", 401);
		
		// All done!
		Log::info("User #{$usr->id} ({$usr->name}) authenticated succesfully");
		return $usr;
	}
	
}
