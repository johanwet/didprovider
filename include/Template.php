<?php
/**
 * The template class.
 * 
 * @author Johan Wettergren
 * @copyright Araneo, 2015
 * @package Support
 */
class Template {
	
	/**
	 * Search a template and replace it with values.
	 * 
	 * @param String Template file.
	 * @param Array|String Replacments.
	 * @return String Replaced template file.
	 */
	public static function replace($template, $array) {
		
		foreach($array as $key => $value) {
			
			$search = "##{$key}##";
			$template = str_replace("##{$key}##", $value, $template);
			
		}

		return $template;
	}
	
}
