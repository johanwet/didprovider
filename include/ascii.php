<?php

/////////////////////
//// ASCII-COLOR ////
/////////////////////

define('ASCII_RESET',  "\033[0m");      // Reset
define('ASCII_BOLD',   "\033[1m");      // Bold
define('ASCII_BLUE',   "\033[0;34m");   // Blue
define('ASCII_BLACK',  "\033[0;30m");   // Black
define('ASCII_GREEN',  "\033[0;32m");   // Green
define('ASCII_CYAN',   "\033[0;36m");   // Cyan
define('ASCII_LCYAN',  "\033[1;36m");   // Cyan
define('ASCII_RED',    "\033[0;31m");   // Red
define('ASCII_LRED',   "\033[1;31m");   // Light Red
define('ASCII_PURPLE', "\033[0;35m");   // Purple
define('ASCII_BROWN',  "\033[0;33m");   // Brown
define('ASCII_LGRAY',  "\033[0;37m");   // Light gray
