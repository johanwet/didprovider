<?php
require_once "../include/loader.php";

// Use cahce file
if(is_file(WSDL_CACHE_FILE) && is_writable(WSDL_CACHE_FILE)) {
	
	// Test the GC (xxx out of 1M)
	if(rand(0,1000000) < WSDL_REGEN_CHANCE) {

		// Load the WSDL template	
		$tpl = file_get_contents(SOAP_WSDL_TEMPLATE);
		
		// Search and Replace the data
		$wsdl = Template::replace($tpl, $GLOBALS['SOAP_VALUES']);
		
		// Write the new cache file	
		file_put_contents(WSDL_CACHE_FILE, $wsdl);		
		
	}
	
	// Don't regen the wsdl
	
// Regenerate the cache file	
} else {
	
	// Load the WSDL template	
	$tpl = file_get_contents(SOAP_WSDL_TEMPLATE);
	
	// Search and Replace the data
	$wsdl = Template::replace($tpl, $GLOBALS['SOAP_VALUES']);
	
	// Write the new cache file	
	file_put_contents(WSDL_CACHE_FILE, $wsdl);
}

// Test if it's WSDL mode
if(isset($_REQUEST['wsdl']) || isset($_REQUEST['WSDL']) || isset($_REQUEST['Wsdl'])) {
	
	Log::debug("(OPERATION) Serving WSDL from cache: " . WSDL_CACHE_FILE);
		
	// Serve the WSDL
	header("Content-type: text/xml; charset=utf-8");
	echo file_get_contents(WSDL_CACHE_FILE);
	exit;
}


try {

	// Create an instance of the API
	$api = new API_SOAP;

	// Create the SOAP server
	$server = new SoapServer(WSDL_CACHE_FILE, 
	                         array("soap_version"   => SOAP_VERSION,
	                               "encoding"       => SOAP_ENCODING,
	                               "cache_wsdl"     => SOAP_CACHE_MODE)
	                         );

	// Apply the host object on the soap server
	$server->setObject($api);
	
	// Handle requests
	Log::debug("Raw Request Data\n" . get_post_data());
	$server->handle(get_post_data());
   	
} catch(Exception $e) {
	Log::exception($e->getCode(), $e->getMessage(), "<operation>");
	$server->fault($e->getCode(), "ErrorCode: " . $e->getCode() . " ErrorMessage: " . $e->getMessage());
	exit;
}
 

/**
 * Get the RAW POST DATA.
 * If $HTTP_RAW_POST_DATA is set, use it. Otherwise read php input.
 * @return string RAW POST DATA.
 */
function get_post_data() {
   return isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : file_get_contents("php://input");
}
