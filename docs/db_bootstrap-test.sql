-- Create Test Users
INSERT INTO user SET name='test-active', company='test', username='test-active', password_hash=sha1(concat('Left4Salt', 'password123', 'Right#Salt')), state='ACTIVE', max_did_locks=1000, created=UTC_TIMESTAMP();
INSERT INTO user SET name='test-active-onedid', company='test', username='test-active-onedid', password_hash=sha1(concat('Left4Salt', 'password123', 'Right#Salt')), state='ACTIVE', max_did_locks=1, created=UTC_TIMESTAMP();
INSERT INTO user SET name='test-disabled', company='test', username='test-disabled', password_hash=sha1(concat('Left4Salt', 'password123', 'Right#Salt')), state='DISABLED', max_did_locks=1000, created=UTC_TIMESTAMP();

-- Create Test DIDs
SET @created=UTC_TIMESTAMP();
SET @state='FREE';

-- SWE:STHLM
INSERT INTO did (country_code, area_code, e164, state, created) VALUES ('SE', '8', '46846500000', @state, @created);
INSERT INTO did (country_code, area_code, e164, state, created) VALUES ('SE', '8', '46846500001', @state, @created);
INSERT INTO did (country_code, area_code, e164, state, created) VALUES ('SE', '8', '46846500002', @state, @created);
INSERT INTO did (country_code, area_code, e164, state, created) VALUES ('SE', '8', '46846500003', @state, @created);
INSERT INTO did (country_code, area_code, e164, state, created) VALUES ('SE', '8', '46846500004', @state, @created);
INSERT INTO did (country_code, area_code, e164, state, created) VALUES ('SE', '8', '46846500005', @state, @created);
INSERT INTO did (country_code, area_code, e164, state, created) VALUES ('SE', '8', '46846500006', @state, @created);
INSERT INTO did (country_code, area_code, e164, state, created) VALUES ('SE', '8', '46846500007', @state, @created);
INSERT INTO did (country_code, area_code, e164, state, created) VALUES ('SE', '8', '46846500008', @state, @created);
INSERT INTO did (country_code, area_code, e164, state, created) VALUES ('SE', '8', '46846500009', @state, @created);
INSERT INTO did (country_code, area_code, e164, state, created) VALUES ('SE', '8', '46846500010', @state, @created);
INSERT INTO did (country_code, area_code, e164, state, created) VALUES ('SE', '8', '46846500011', @state, @created);
INSERT INTO did (country_code, area_code, e164, state, created) VALUES ('SE', '8', '46846500012', @state, @created);
INSERT INTO did (country_code, area_code, e164, state, created) VALUES ('SE', '8', '46846500013', @state, @created);
INSERT INTO did (country_code, area_code, e164, state, created) VALUES ('SE', '8', '46846500014', @state, @created);
INSERT INTO did (country_code, area_code, e164, state, created) VALUES ('SE', '8', '46846500015', @state, @created);
INSERT INTO did (country_code, area_code, e164, state, created) VALUES ('SE', '8', '46846500016', @state, @created);
INSERT INTO did (country_code, area_code, e164, state, created) VALUES ('SE', '8', '46846500017', @state, @created);
INSERT INTO did (country_code, area_code, e164, state, created) VALUES ('SE', '8', '46846500018', @state, @created);
INSERT INTO did (country_code, area_code, e164, state, created) VALUES ('SE', '8', '46846500019', @state, @created);
INSERT INTO did (country_code, area_code, e164, state, created) VALUES ('SE', '8', '46846500020', @state, @created);
INSERT INTO did (country_code, area_code, e164, state, created) VALUES ('SE', '8', '46846500021', @state, @created);
INSERT INTO did (country_code, area_code, e164, state, created) VALUES ('SE', '8', '46846500022', @state, @created);
INSERT INTO did (country_code, area_code, e164, state, created) VALUES ('SE', '8', '46846500023', @state, @created);
INSERT INTO did (country_code, area_code, e164, state, created) VALUES ('SE', '8', '46846500024', @state, @created);
INSERT INTO did (country_code, area_code, e164, state, created) VALUES ('SE', '8', '46846500025', @state, @created);

-- SWE:GBG
INSERT INTO did (country_code, area_code, e164, state, created) VALUES ('SE', '31', '46315070100', @state, @created);
INSERT INTO did (country_code, area_code, e164, state, created) VALUES ('SE', '31', '46315070101', @state, @created);
INSERT INTO did (country_code, area_code, e164, state, created) VALUES ('SE', '31', '46315070102', @state, @created);
INSERT INTO did (country_code, area_code, e164, state, created) VALUES ('SE', '31', '46315070103', @state, @created);
INSERT INTO did (country_code, area_code, e164, state, created) VALUES ('SE', '31', '46315070104', @state, @created);
INSERT INTO did (country_code, area_code, e164, state, created) VALUES ('SE', '31', '46315070105', @state, @created);
INSERT INTO did (country_code, area_code, e164, state, created) VALUES ('SE', '31', '46315070106', @state, @created);
INSERT INTO did (country_code, area_code, e164, state, created) VALUES ('SE', '31', '46315070107', @state, @created);
INSERT INTO did (country_code, area_code, e164, state, created) VALUES ('SE', '31', '46315070108', @state, @created);
INSERT INTO did (country_code, area_code, e164, state, created) VALUES ('SE', '31', '46315070109', @state, @created);

-- NOR:no-geo
INSERT INTO did (country_code, area_code, e164, state, created) VALUES ('NO', '', '4720315020', @state, @created);
INSERT INTO did (country_code, area_code, e164, state, created) VALUES ('NO', '', '4720315021', @state, @created);
INSERT INTO did (country_code, area_code, e164, state, created) VALUES ('NO', '', '4720315022', @state, @created);
INSERT INTO did (country_code, area_code, e164, state, created) VALUES ('NO', '', '4720315023', @state, @created);
INSERT INTO did (country_code, area_code, e164, state, created) VALUES ('NO', '', '4720315024', @state, @created);
INSERT INTO did (country_code, area_code, e164, state, created) VALUES ('NO', '', '4720315025', @state, @created);
INSERT INTO did (country_code, area_code, e164, state, created) VALUES ('NO', '', '4720315026', @state, @created);
INSERT INTO did (country_code, area_code, e164, state, created) VALUES ('NO', '', '4720315027', @state, @created);
INSERT INTO did (country_code, area_code, e164, state, created) VALUES ('NO', '', '4720315028', @state, @created);
INSERT INTO did (country_code, area_code, e164, state, created) VALUES ('NO', '', '4720315029', @state, @created);
