DROP DATABASE IF EXISTS lisa_did;
CREATE DATABASE lisa_did DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;

USE lisa_did;

CREATE TABLE user (
  id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(64) NOT NULL,
  company VARCHAR(64) NOT NULL,
  username VARCHAR(32) NOT NULL,
  password_hash CHAR(40) NOT NULL,
  state ENUM('ACTIVE', 'DISABLED', 'TERMINATED') NOT NULL DEFAULT 'ACTIVE',
  max_did_locks SMALLINT UNSIGNED NOT NULL,
  created DATETIME NOT NULL,
  modified DATETIME DEFAULT NULL,
  
  UNIQUE INDEX (username),
  INDEX (username, password_hash, state)

) ENGINE=InnoDB CHARACTER SET=utf8 COLLATE=utf8_general_ci COMMENT='Authorized API Users';

CREATE TABLE did (
  id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  country_code CHAR(2) NOT NULL, 
  area_code VARCHAR(4) NOT NULL DEFAULT '', 
  e164 VARCHAR(18) NOT NULL, 
  owner INT UNSIGNED DEFAULT NULL, 
  state ENUM('FREE', 'LOCKED', 'IN_USE', 'RESERVED', 'QUARANTINED', 'PORTED_OUT') NOT NULL DEFAULT 'FREE',
  created DATETIME NOT NULL,
  modified DATETIME DEFAULT NULL,
  
  FOREIGN KEY (owner) REFERENCES user(id),
  
  UNIQUE INDEX(e164),  
  INDEX (state, country_code, area_code)

) ENGINE=InnoDB CHARACTER SET=utf8 COLLATE=utf8_general_ci COMMENT='Holds DIDs';

CREATE TABLE didlock (
  id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  did_id INT UNSiGNED NOT NULL,
  locked_by INT UNSIGNED NOT NULL,
  lock_key VARCHAR(36) NOT NULL,
  released_at DATETIME NOT NULL,
  created DATETIME NOT NULL,
  modified DATETIME DEFAULT NULL,

  FOREIGN KEY (did_id) REFERENCES did(id),
  FOREIGN KEY (locked_by) REFERENCES user(id),
  
  UNIQUE INDEX (did_id),
  INDEX (released_at),
  INDEX (lock_key, locked_by, did_id)
  
 ) ENGINE=InnoDB CHARACTER SET=utf8 COLLATE=utf8_general_ci COMMENT='Temporary DID Locks';
 