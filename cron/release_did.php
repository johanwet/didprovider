#!/usr/bin/php
<?php
/**
 * Release all DID Locks which has expired.
 * 
 * @author Johan Wettergren
 * @copyright Araneo, 2015
 * @package Cron
 */

require_once "../include/loader.php";

Log::info("DIDLock Release script started");

try {
	
	// Get all DIDLocks to expire, and delete them
	foreach(DIDLock::getExpired() as $expDid)
		$expDid->__delete();

	Log::info("DIDLock Release script completed successfully");
	exit(0);
	
} catch(SQLException $sqlEx) {
	Log::error("Failed to Release DIDLock, SQLException: " . $sqlEx->getMessage());
	exit(3);
	
} catch(Exception $ex) {
	Log::error("Failed to Release DIDLock, Exception: " . $ex->getMessage());
	exit(4);
}
