<?php

// Database connection
define("DB_HOST",				"lisa-did.ch8mabvb4u6l.us-east-1.rds.amazonaws.com");
define("DB_USER",				"lisa-soap");
define("DB_PASSWORD",			"secret.321");
define("DB_DBASE",				"lisa_did");
define("DB_PORT",				3306);
define("DB_USE_SSL",			false);
define("DB_SSL_PEM",			"");

// Memcache settings
define("MC_SERVER_URI",			null);
define("MC_SERVER_PORT",		null);

// Log settings
define("LOG_TEXT_FILE",			"/tmp/lisa-didprovider.log");
define("LOG_DATE_FORMAT",		"M-j H:i:s");
define("LOG_DBL_DEBUG_MODE",	true);
define("LOG_DEBUG_MODE",		true);

// SOAP Device Server Settings
define("SOAP_ENDPOINT",			"http://127.0.0.1/operation.php?wsdl");
define("SOAP_VERSION",			1);
define("SOAP_ENCODING",			"utf-8");
define("SOAP_CACHE_MODE",		0);
define("WSDL_CACHE_FILE",		"/tmp/lisasoap-wsdl-file-do-no-alter");
define("SOAP_WSDL_TEMPLATE",	"../wsdl/didprovider.wsdl");
define("WSDL_REGEN_CHANCE",		1000000); // 100% regen

$GLOBALS['SOAP_VALUES']  = array('PROTO'		=> "http",
								 'HOST'			=> "127.0.0.1",
								 'FILEPATH'		=> "operation.php",
								 'NAMESPACE'	=> "DIDSoapProxy",
								 'ENCODING'		=> "utf-8",
								 'VERSION'		=> "1.0");
