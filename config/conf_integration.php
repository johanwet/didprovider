<?php

// Database connection
define("DB_HOST",				"localhost");
define("DB_USER",				"app_didprovider");
define("DB_PASSWORD",			"prodsecret-3211");
define("DB_DBASE",				"didprovider");
define("DB_PORT",				3306);
define("DB_USE_SSL",			false);
define("DB_SSL_PEM",			"");

// Memcache settings
define("MC_SERVER_URI",			null);
define("MC_SERVER_PORT",		null);

// Log settings
define("LOG_TEXT_FILE",			"/var/log/didprovider/didprovider-server.log");
define("LOG_DATE_FORMAT",		"M-j H:i:s");
define("LOG_DBL_DEBUG_MODE",	true);
define("LOG_DEBUG_MODE",		true);

// SOAP Device Server Settings
define("SOAP_ENDPOINT",			"http://integration.araneo.se/operation.php?wsdl");
define("SOAP_VERSION",			SOAP_1_1);
define("SOAP_ENCODING",			"utf-8");
define("SOAP_CACHE_MODE",		WSDL_CACHE_NONE);
define("WSDL_CACHE_FILE",		"/tmp/lisasoap-wsdl-file-do-no-alter");
define("SOAP_WSDL_TEMPLATE",	"../wsdl/didprovider.wsdl");
define("WSDL_REGEN_CHANCE",		1000000); // 100% regen

$GLOBALS['SOAP_VALUES']  = array('PROTO'		=> "http",
								 'HOST'			=> "integration.araneo.se",
								 'FILEPATH'		=> "operation.php",
								 'NAMESPACE'	=> "DIDSoapProxy",
								 'ENCODING'		=> "UTF-8",
								 'VERSION'		=> "1.0");
