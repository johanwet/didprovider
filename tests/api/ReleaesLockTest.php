<?php
/**
 * API Tests related to Relasing locked DIDs.
 * 
 * @author Johan Wettergren
 * @copyright Araneo, 2015
 */
class ReleaesLockTest extends APITestBase {

	/**
	 * Create a Lock for 5 DIDs, don't Allocate any and Release.
	 * You should get 5 DIDs released.
	 * 
	 * @group smoke
	 */
	public function testReleaseLockedOnly() {
		
		Log::fcn("ReleaesLockTest::testReleaseLockedOnly()");

		$api = self::getClient("test-active", "password123");
		
		// There should exist 5 Free DIDs in Stockholm/Sweden
		$objDid = $api->__call("lockRange", array("countryCode" => "SE", "areaCode" => "8", "amount" => 5));
		$lockKey = $objDid->lockKey;
		
		// Release them right away!
		$numReleased = $api->__call("releaseLock", array("key" => $lockKey));
		
		// Make sure we released all 5 DIDs!
		$this->assertEqual($numReleased, 5);		
	}

	
	/**
	 * Create a Lock for 5 DIDs, Allocate one, and Release.
	 * You should get 4 DIDs released.
	 * 
	 * @group smoke
	 */
	public function testReleaseLockedAllocated() { 
		
		Log::fcn("ReleaesLockTest::testReleaseLockedAllocated()");		
		
		$api = self::getClient("test-active", "password123");
		
		// There should exist 5 Free DIDs in Stockholm/Sweden
		$objDid = $api->__call("lockRange", array("countryCode" => "SE", "areaCode" => "8", "amount" => 5));
		$lockKey = $objDid->lockKey;
		
		// Grab one of the available DIDs and the Lock Key
		$did = $objDid->dids[array_rand($objDid->dids, 1)];
		$lockKey = $objDid->lockKey;
		Log::debug(("DID to Allocate. #{$did->id} e.164: {$did->e164} using key: {$lockKey}"));

		// Allocate the DID
		$retDid = $api->__call("allocateDID", array("didId" => $did->id, "key" => $lockKey));
		Log::debug("Allocated DID: " . print_r($retDid, true));		
		
		// Release the remaining ones
		$numReleased = $api->__call("releaseLock", array("key" => $lockKey));
		
		// Now we should have released 4 of the DIDs, since one is consumed
		$this->assertEqual($numReleased, 4);
	}

	
	/**
	 * Release using a non-existing Lock Key.
	 * You should get 0 back.
	 * 
	 */
	public function testReleaseUsingInvalidKey() { 
		
		Log::fcn("ReleaesLockTest::testReleaseUsingInvalidKey()");		
		
		$api = self::getClient("test-active", "password123");

		// Release the remaining ones
		$numReleased = $api->__call("releaseLock", array("key" => unique('test-', true)));

		// Should be 0 DIDs released by this Key
		$this->assertEqual($numReleased, 0);		
	}

	
	/**
	 * Create a Lock with one Active user, Try to relase it with the correct Lock Key
	 * using another User's privilege.
	 * You should get 0 back.
	 *
	 */
	public function testReleaseWithDifferentUser() {
		
		Log::fcn("ReleaesLockTest::testReleaseWithDifferentUser()");

		$api = self::getClient("test-active", "password123");
		
		// There should exist 5 Free DIDs in Stockholm/Sweden
		$objDid = $api->__call("lockRange", array("countryCode" => "SE", "areaCode" => "8", "amount" => 5));
		$lockKey = $objDid->lockKey;
		
		// Create an API connection object with another active user
		$api2 = self::getClient("test-active-one", "password123");		
		
		// Release them with the second active user
		$numReleased = $api2->__call("releaseLock", array("key" => $lockKey));
		
		$this->assertEqual($numReleased, 0);
	}

	
	/**
	 * Try to Release an (invalid) DID Lock with a Disabled user.
	 * You should get 403/Forbidden back.
	 * 
	 * @expectedException	SoapFault
	 */
	public function testReleaseWithDisabledUser() {
		
		Log::fcn("ReleaesLockTest::testReleaseWithDifferentUser()");		

		$api = self::getClient("test-disabled", "password123");
				
		// Release them with the second active user
		$numReleased = $api->__call("releaseLock", array("key" => "doesnotmatterreally"));
	}	
	
}

