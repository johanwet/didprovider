<?php
/**
 * API Tests related to Allocating DIDs once they have been Locked.
 * 
 * @author Johan Wettergren
 * @copyright Araneo, 2015
 * @see LockDIDRangeTest
 */
class AllocateDIDTest extends APITestBase {
		
	/**
	 * Create a Lock and Allocate a DID from it. 
	 * 
	 * @group smoke
	 */
	public function testAllocateInValidRegion() {

		Log::fcn("AllocateDIDTest::testAllocateInValidRegion()");
		
		$api = self::getClient("test-active", "password123");
		
		// There should exist 5 Free DIDs in Stockholm/Sweden
		$objDid = $api->__call("lockRange", array("countryCode" => "SE", "areaCode" => "8", "amount" => 5));
		Log::debug("SOAP response for Stockholm: " . print_r($objDid, true));
				
		// Grab one of the available DIDs and the Lock Key
		$did = $objDid->dids[array_rand($objDid->dids, 1)];
		$lockKey = $objDid->lockKey;
		Log::debug(("DID to Allocate. #{$did->id} e.164: {$did->e164} using key: {$lockKey}"));

		// Allocate the DID
		$retDid = $api->__call("allocateDID", array("didId" => $did->id, "key" => $lockKey));
		Log::debug("Allocated DID: " . print_r($retDid, true));
		
		// Make sure it is IN_USE now
		$this->assertEquals($retDid->state, "IN_USE");
		
		// Make sure it's the right DID which is locked
		$this->assertEquals($retDid->id, $did->id);
		$this->assertEquals($retDid->e164, $did->e164);
	}

	
	/**
	 * Try allocating a validly locked DID but using the wrong Lock Key
	 * 
	 * @expectedException	SoapFault
	 */
	public function testAllocateWithInvalidKey() { 
		
		Log::fcn("AllocateDIDTest::testAllocateWithInvalidKey()");
		
		$api = self::getClient("test-active", "password123");
		
		// Grab 3 DIDs from a valid Reagion
		$objDid = $api->__call("lockRange", array("countryCode" => "SE", "areaCode" => "8", "amount" => 3));
		Log::debug("SOAP response for Stockholm: " . print_r($objDid, true));
		
		// Grab one of the available DIDs and the Lock Key
		$did = $objDid->dids[array_rand($objDid->dids, 1)];
		$lockKey = $objDid->lockKey;
		Log::debug(("DID to Allocate. #{$did->id} e.164: {$did->e164} using key: {$lockKey}"));
				
		// Allocate the DID using a Bogus Key
		$retDid = $api->__call("allocateDID", array("didId" => $did->id, "key" => uniqid('test-', true)));		
	}

	
	/**
	 * Try to allocate a DID using an ID which doesn't exist.
	 * 
	 * @expectedException	SoapFault
	 */
	public function testAllocateWithInvalidDIDId() { 
		
		Log::fcn("AllocateDIDTest::testAllocateWithInvalidDIDId()");
		
		$api = self::getClient("test-active", "password123");
		
		// Grab 3 DIDs from a valid Reagion
		$objDid = $api->__call("lockRange", array("countryCode" => "SE", "areaCode" => "8", "amount" => 3));
		Log::debug("SOAP response for Stockholm: " . print_r($objDid, true));
		
		// Grab one of the available DIDs and the Lock Key
		$did = $objDid->dids[array_rand($objDid->dids, 1)];
		$lockKey = $objDid->lockKey;
		Log::debug(("DID to Allocate. #{$did->id} e.164: {$did->e164} using key: {$lockKey}"));
		
		// Allocate the DID using an invalid DID ID
		$didId = PHP_INT_MAX - 1; // this auto_inc hardly exists in the DID Database
		$retDid = $api->__call("allocateDID", array("didId" => $didId, "key" => "doesnotmatter"));		
	}
	
	
	/**
	 * Try allocating a non-existing DID using a disabled user.
	 * Should return 403/Forbidden.
	 * 
	 * @expectedException	SoapFault
	 */
	public function testAllocateWithDisabledUser() {
		
		Log::fcn("AllocateDIDTest::testAllocateInValidRegion()");
		
		$api = self::getClient("test-disabled", "password123");
				
		// Allocate the DID
		$retDid = $api->__call("allocateDID", array("didId" => 1, "key" => "doesNoteMatter"));
		Log::debug("Allocated DID: " . print_r($retDid, true));				
	}
	
}
