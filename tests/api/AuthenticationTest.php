<?php
/**
 * API Tests of User Authentication.
 * 
 * @author Johan Wettergren
 * @copyright Araneo, 2015
 */
class AuthenticationTest extends APITestBase {
	
	/**
	 * Login with a valid username and password for an active account.
	 * 
	 * @group smoke
	 */
	public function testValidActiveAuthenticaiton() {

		Log::fcn("AuthenticationTest::testValidActiveAuthenticaiton()");
		
		// This is an active account authentication
		$api = self::getClient("test-active", "password123");

		// Use the dummy do-nothing method
		$obj = $api->__call("ping", array());
		Log::debug("Pong object: " . print_r($obj, true));

		// Just check that the reponses exists
		$this->assertObjectHasAttribute('quiterandom', $obj);
		$this->assertObjectHasAttribute('servertime', $obj);
		$this->assertObjectHasAttribute('servertz', $obj);
	}
	
	
	/**
	 * Provide a valid username, but an invalid password to an Active account.
	 *
	 * @expectedException	SoapFault
	 */
	public function testValidUserInvalidPassword() {
	
		Log::fcn("AuthenticationTest::testValidUserInvalidPassword()");
	
		// This is an active account authentication
		$api = self::getClient("test-active", "thisIsNotTheRightPasswordForSure");
	
		// Use the dummy do-nothing method
		$obj = $api->__call("ping", array());
	
	}
	
	/**
	 * This account exists, but is currently not enabled (not in state ACTIVE)
	 * 
	 * @expectedException	SoapFault
	 */
	public function testDisabledAuthenticaiton() {
	
		Log::fcn("AuthenticationTest::testDisabledAuthenticaiton()");
	
		// This is user exists but is disabbled
		$api = self::getClient("test-disabled", "password123");
	
		// Use the dummy do-nothing method
		$obj = $api->__call("ping", array());
	}	
	

	/**
	 * Test to supply a Username which does not exist in the system for sure.
	 * 
	 * @expectedException	SoapFault
	 */
	public function testNonExistingAuthenticaiton() {
	
		Log::fcn("AuthenticationTest::testNonExistingAuthenticaiton()");
	
		// This user will not exist for sure!
		$api = self::getClient(uniqid('foo', true), "doesntmatter");
	
		// Use the dummy do-nothing method
		$obj = $api->__call("ping", array());
	}
	
}
