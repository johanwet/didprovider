<?php
/**
 * Test the Lock Range method in the SOAP API.
 * This is the entry-method which will start the Transaction on success.
 * 
 * @author Johan Wettergren
 * @copyright Araneo, 2015
 */
class LockDIDRangeTest extends APITestBase {
			
	/**
	 * Lock an allowed number of DIDs in an exising Region which has sufficent amount of Free DIDs
	 * 
	 * @group smoke
	 */
	public function testLockInValidRegion() {

		Log::fcn("LockDIDRangeTest::testLockInValidRegion()");
		
		$api = self::getClient("test-active", "password123");

		// There should exist 5 Free DIDs in Stockholm/Sweden
		$objDid = $api->__call("lockRange", array("countryCode" => "SE", "areaCode" => "8", "amount" => 5));
		Log::debug("SOAP response for Stockholm: " . print_r($objDid, true));
				
		// Validate: did (DID array)
		$this->assertObjectHasAttribute('dids', $objDid);
		$this->assertTrue(is_array($objDid->dids), "Valid did response list should contain 'dids' array");
		$this->assertEquals(5, count($objDid->dids));
		
		// Validate: lockKey (GUID)
		$this->assertObjectHasAttribute('lockKey', $objDid);
		$this->assertRegExp('/^[a-f0-9\-]{36}$/', $objDid->lockKey);
	}

	
	/**
	 * Try to create a DID Lock in a Region which does not exust.
	 * This should not throw any error, just an empty List.
	 * 
	 * @group smoke
	 */
	public function testLockInInvalidRegion() {

		Log::fcn("LockDIDRangeTest::testLockInInvalidRegion()");
		
		$api = self::getClient("test-active", "password123");
	
		// Try to allocate 5 DIDs in Sweden/Växjö  - should not exist
		$objDid = $api->__call("lockRange", array("countryCode" => "SE", "areaCode" => "470", "amount" => 5));
		Log::debug("SOAP response for Vaxjo: " . print_r($objDid, true));
		
		// This should just be an empty object, no attributes, due to PHP SOAP's object parsing. 
		$this->assertTrue(is_object($objDid), "Empty lockRange set should be object");
		$this->assertFalse(isset($objDid->dids), "Emptry lockRange set should not contain 'dids' attribute");
		
		// Validate: lockKey (GUID)
		$this->assertObjectHasAttribute('lockKey', $objDid);
		$this->assertRegExp('/^[a-f0-9\-]{36}$/', $objDid->lockKey);		
	}

	
	/**
	 * Test allocating 5000 DIDs in a Region. This is not allowed (too many).
	 * 
	 * @expectedException				SoapFault
	 * @expectedExceptionMessageRegExp	/Trying to allocate too many DIDs at the same time/
	 */
	public function testLockTooLargeChunk() {
		
		Log::fcn("LockDIDRangeTest::testLockTooLargeChunk()");
				
		$api = self::getClient("test-active", "password123");

		// You can't allocate 5000 DIDs in a chunk!
		$objDid = $api->__call("lockRange", array("countryCode" => "SE", "areaCode" => "8", "amount" => 5000));
	}
	
	
	/**
	 * Try locking a DID Range with a user which exists but is Disabled.
	 * Should return a 403/Forbidden.
	 * 
	 * @expectedException	SoapFault
	 */
	public function testLockWithDisabledUser() {

		Log::fcn("LockDIDRangeTest::testLockWithDisabledUser()");
		
		$api = self::getClient("test-disabled", "password123");
		
		// You can't allocate 5000 DIDs in a chunk!
		$objDid = $api->__call("lockRange", array("countryCode" => "SE", "areaCode" => "8", "amount" => 5));		
	}

	
	/**
	 * Try locking a DID Range with a user which does not exist..
	 * Should return a 403/Forbidden.
	 *
	 * @expectedException	SoapFault
	 */
	public function testLockWithNonExistingUser() {
	
		Log::fcn("LockDIDRangeTest::testLockWithDisabledUser()");
	
		$api = self::getClient(uniqid('test-', true), "doesntreallymatter");
	
		// You can't allocate 5000 DIDs in a chunk!
		$objDid = $api->__call("lockRange", array("countryCode" => "SE", "areaCode" => "8", "amount" => 5));
	}
		
}

