# DID Provider Demo Project #
This is a project used to demonstrate Contentious Integration using Atlassian Bamboo. It includes automated testing, code quality analysis, external load/performance testing, automation of API documentaion, and much more.  

[Link to CI Article](http://www.araneo.se/continuous-integration-in-theory--practice.html)

## About the Project ##
The project is simulating a DID Provider, that is, a supplier of phone numbers. The DID Provider holds large pools of phone numbers in multiple countries, which clients can purchase for a nominal fee. To purchase a DID, you must first lock a "range" of numbers. This lock exists for 15 minutes (can be up for change). Each lock has a unique lock code. From the range of locked numbers you are allowed to pick one number, which you allocate with an API call together with the lock code. The non-chosen numbers are returned to the "number pool" of available numbers.

It's based on a LAMP-stack using:
* CentOS 6.6
* PHP 5.3
* MySQL 5.6
* Apache 2.2

You can also check out [my article](http://www.araneo.se/to-make-a-virtual-lamp-development-environment.html) on how to setup a virtual LAMP-stack on VirtualBox.

## Code Base ##
You probably want the development branch...